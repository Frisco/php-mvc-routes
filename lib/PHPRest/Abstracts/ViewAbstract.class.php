<?php

namespace PHPRest\Abstracts;

use PHPRest\Interfaces as Interfaces;

abstract class ViewAbstract implements Interfaces\ViewInterface{
	abstract function render( $data ); // Extending class must implement an override for this function
}

<?php

namespace PHPRest\Interfaces;

interface ControllerInterface{
	public function __construct( $these_controller_vars, $these_parameters, $this_view_format );
}

<?php

namespace PHPRest\Interfaces;

interface ModelInterface{
	public function createData( $request );		// C: Save 		information 	to the data store.
	public function readData( $request );		// R: Read 		information 	from the data store.
	public function updateData( $request );		// U: Update 	information 	in the data store.
	public function deleteData( $request );		// C: Delete 	information 	from the data store.
}

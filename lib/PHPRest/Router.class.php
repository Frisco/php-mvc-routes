<?php

namespace PHPRest;

class Router{

	private $_verb;
	private $_url;
	private $_view_format;
	private $_parameters;
	private $_routes;

	public function __construct(){

		$this->_routes = array();

		spl_autoload_register( 'PHPRest\Router::loadClass' );

		$this->_verb = $_SERVER[ 'REQUEST_METHOD' ];
		$this->_url = $_SERVER[ 'PATH_INFO' ];

		$this->_parseIncomingParams();
		$this->_view_format = 'json'; //JSON is the default format
		if( isset( $this->_parameters[ 'format' ] ) ){
			$this->_view_format = $this->_parameters[ 'format' ];
		}

		return true;

	}

	public function loadClass( $this_classname ){
		if( strlen( $this_classname) > 0 ){
			$class_path = "";

			switch( true ){
				case preg_match( '/[a-zA-Z]+Interface$/', $this_classname ) :
					// Load the PHPRest Interface classes from the library
					$class_path = './lib/PHPRest/Interfaces/' .
												substr( $this_classname, strrpos( $this_classname, '\\' ) + 1 ) .
												'.class.php';
					break;
				case preg_match( '/[a-zA-Z]+Abstract$/', $this_classname ) :
					// Load the PHPRest Abstract classes from the library
					$class_path = './lib/PHPRest/Abstracts/' .
												substr( $this_classname, strrpos( $this_classname, '\\' ) + 1 ) .
												'.class.php';
					break;
				case preg_match( '/[a-zA-Z]+Controller$/', $this_classname ) :
					// Load any custom Controller classes from the controllers project folder
					$class_path =  './controllers/' . $this_classname . '.class.php';
					break;
				case preg_match( '/[a-zA-Z]+Model$/', $this_classname ) :
					// Load any custom Model classes from the models project folder
					$class_path =  './models/' . $this_classname . '.class.php';
					break;
				case preg_match( '/[a-zA-Z]+View$/', $this_classname ) :
					// Load any custom Views classes from the views project folder
					$class_path =  './views/' . $this_classname . '.class.php';
					break;
			}

			include $class_path;
		}
	}

	public function startWatch(){

		$pregs = $this->make_pregs();
		for( $i = 0; $i < count( $pregs['tests'] ); $i++ ){
			if( preg_match( "/" . $pregs['tests'][$i] . "/", $this->_url ) ){
				$controller_vars = preg_replace(  "/" . $pregs['tests'][$i] . "/", $pregs['solutions'][$i], $this->_url );
				if( $controller_vars != "{}" ){
					$controller_vars = json_decode( $controller_vars );
				}else{
					$controller_vars = null;
				}
				$controller_name = ucfirst( $pregs['controllers'][$i] );
				if( class_exists( $controller_name ) ){
					$page_controller = new $controller_name( $controller_vars, $this->_parameters, $this->_view_format );
				}
				break;
			}
		}

	}


	public function route( $this_route_path, $this_controller ){

		$this_route = array();
		$this_route_vars = array();
		$these_route_elements = explode('/', $this_route_path );

		for( $i = 0; $i < count( $these_route_elements ); $i++ ){
			$is_var = strpos( $these_route_elements[$i], ':');
			if( $is_var !== false ){
				array_push( $this_route_vars, substr( $these_route_elements[$i], strpos( $these_route_elements[$i], ':') ) );
				$these_route_elements[$i] = "\:([a-zA-Z1-9_][^\/]+)";
			}
		}

		$this_route['controller_name'] = $this_controller;
		$this_route['test'] = implode( "\/", $these_route_elements );
		$this_route['params'] = $this_route_vars;

		array_push( $this->_routes, $this_route );

	}

	private function make_pregs(){
		$these_pregs = array();
		$these_pregs['tests'] = array();
		$these_pregs['solutions'] = array();
		$these_pregs['controllers'] = array();
		for( $i = 0; $i < count( $this->_routes ); $i++ ){
			array_push( $these_pregs['tests'], $this->_routes[$i]['test'] );
			$solution = '';
			for( $j = 0; $j < count( $this->_routes[$i]['params'] ); $j++ ){
				$solution .= '"' . $this->_routes[$i]['params'][$j] . '": $' . $j . ', ';
			}
			array_push( $these_pregs['solutions'], '{' . $solution . '}' );
			array_push( $these_pregs['controllers'],  $this->_routes[$i]['controller_name'] );
		}
		return $these_pregs;
	}

	private function _parseIncomingParams(){

		$these_parameters = array();

		// GET METHOD first
		if( isset( $_SERVER[ 'QUERY_STRING'] ) ){
			parse_str( $_SERVER[ 'QUERY_STRING'], $these_parameters );
		}

		// then PUT or POST
		$this_data = file_get_contents('php://input');
		$this_content_type = false;
		if( isset( $_SERVER[ 'CONTENT_TYPE' ] ) ){
			$this_content_type = $_SERVER[ 'CONTENT_TYPE' ];
		}
		switch( $this_content_type ){

			case 'application/json' :

				$data_params = json_decode( $this_data );
				if( $data_params ){
					foreach( $data_params as $param_id => $param_val ){
						$these_parameters[ $param_id ] = $param_val;
					}
				}
				$this->_view_format = 'json';
				break;

			case 'application/x-www-form-urlencoded':

				parse_str( $this_data, $post_vars );
				foreach( $post_vars as $post_var => $var_val ){
					$these_parameters[ $post_var ] = $var_val;
				}
				$this->_view_format = 'html';
				break;

			default:

				//more formats here

		}

		$this->_parameters = $these_parameters;

	}

}

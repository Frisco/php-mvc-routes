<?php
		include_once( './lib/PHPRest/Router.class.php' );

		$objRouter = new PHPRest\Router();

		$objRouter->route("/api/classes","CoursesController");
		$objRouter->route("/api/classes/:class_id","CoursesController");
		$objRouter->startWatch();
